/** jQuery easing from: http://gsgd.co.uk/sandbox/jquery/easing/jquery.easing.1.3.js */
jQuery.easing['jswing'] = jQuery.easing['swing'];

jQuery.extend( jQuery.easing, {
	def: 'easeInCubic',
	easeOutCubic: function (x, t, b, c, d) {
		return c*((t=t/d-1)*t*t + 1) + b;
	}
});


$(document).ready(function() {
	var $scrollToLinks = $('a[data-scrollto]');
	var $htmlBodyElms = $('html, body');

	$scrollToLinks.on('click', function (e) {
		e.preventDefault();

		$htmlBodyElms.animate({ scrollTop: $(this).data('scrollto') }, 'normal', 'easeOutCubic');
	});
});


/*$(document).on('scroll', function () {
	var items = $('.item')
	var scrollPosition = $(this).scrollTop();
  	var itemPosition = [];

	for (var i = 0; i < items.length; i++ ) {
		itemPosition.push(items[i].offsetTop);
	};

	for (var i = 0; i < items.length; i++) {
		if ( scrollPosition > itemPosition[i] + ((itemPosition[i + 1] - itemPosition[i]) / 2) ) {
			$(items[i + 1]).addClass('active');
		}else{
			$(items[i + 1]).removeClass('active');
		};
	}
	console.log($(window).scrollTop());
});*/